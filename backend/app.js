'use strict'

var express = require('express');
var bodyParser = require('body-parser');
var projectRoutes = require('./routes/persona');
var productroutes = require('./routes/producto');  //14092022

var app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY', 'Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();

});

app.use('/api/persona',projectRoutes);
app.use('/api/producto',productroutes);              //14092022

module.exports = app;

// app.get('/prueba',(req,res)=> {
//     res.status(200).send({
//         message: "Servicio GET con NodeJS"
//     });
// });

// app.get('/',(req,res)=> {
//     res.status(200).send({
//         message: "Servicio GET 2 con NodeJS"
//     });
// });

// app.post('/prueba/:key',(req,res)=> {
//     console.log('nombre ',req.body.nombre);
//     console.log('query ',req.query.id);
//     console.log('path ',req.params.key);
//     res.status(200).send({
//         message: "Servicio POST con NodeJS"
//     });    
// })



