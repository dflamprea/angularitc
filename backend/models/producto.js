//14092022
'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductoSchema = Schema ({
    name        : String,
    description : String,
    valor       : Number,
    year        : Number,
    imageFile : String
});

module.exports = mongoose.model('Producto',ProductoSchema);