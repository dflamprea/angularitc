'use strict'

var Persona = require('../models/persona');
var filesystem = require('fs');
var path = require('path');

var controller = {
    home: function(req,res){
        return res.status(200).send({
            message: "Home"
        });
    },

    test: function(req,res){
        return res.status(200).send({
            message: "Test"
        });
    },

    savePerson: function (req,res){
        var persona = new Persona();

        var request = req.body;
        persona.names     = request.names     ;  // String,
        persona.lastNames = request.lastNames ;  // String,
        persona.address   = request.address   ;  // String,
        persona.phone     = request.phone     ;  // String,
        persona.birthDate = request.birthDate ;  // String

        persona.save((error,personStored)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error almacenando la persona"
            });
            if (!personStored) return res.status(404).send({
                message: "No se pudo almacenar la persona"
            });
            return res.status(200).send({
                persona: personStored
            });
        });
    },

    getPersonById: function(req, res){
        var personId = req.params.id;

        Persona.findById(personId, (error,persona)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error consultando la persona"
            });
            if (!persona) return res.status(204).send({
                message: "No se pudo encontrar la persona con este id" + personId
            });
            return res.status(200).send({
                persona
            });
        });
    },

    uploadImage: function(req, res){

        if (req.files) {
            var personId = req.params.id;
            var path = req.files.image.path;
            var fSplit = path.split('\\');
            var fileName = fSplit[1];
            console.log(path);
            console.log(fileName);
    
            Persona.findByIdAndUpdate(personId, {imageFile:fileName}, (error, personaUpdated)=>{
                if (error) return res.status(500).send({
                    message: "Ha ocurrido un error actualizando la imagen"
                });
                if (!personaUpdated) return res.status(204).send({
                    message: "No se pudo encontrar la persona con este id" + personId
                });
                return res.status(200).send({
                    message: "la persona con id" + personId + "fue actualizada"
                });         
            });
        } else {
            return res.status(202).send({
                message: 'Procesando la solicitud'
            });
        }
    },

    getImageByFileName : function(req, res){
        var fileName = req.params.fileName;
        var pathFile = './uploads/' + fileName;

        filesystem.exists(pathFile,(exists) => {
            if(exists) {
                return res.sendFile(path.resolve(pathFile));
            } else {
                return res.status(204).send({
                    message: "error"
                })
            }
        });
    }


};

module.exports = controller;

