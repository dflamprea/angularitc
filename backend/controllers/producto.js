//14092022
'use strict'

const producto = require('../models/producto');
var Producto = require('../models/producto')

var controller = {

    saveProduct: function (req,res){
        var producto = new Producto();

        var request = req.body;
        producto.name        = request.name        ;// String,
        producto.description = request.description ;// String,
        producto.valor       = request.valor       ;// Number,
        producto.year        = request.year        ;// Number,

        producto.save((error,productoStored)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error almacenando el producto"
            });
            if (!productoStored) return res.status(404).send({
                message: "No se pudo almacenar el producto"
            });
            return res.status(200).send({
                producto: productoStored
            });
        });
    },

    allProduct: function (req,res){
        Producto.find((error,productos)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error consultando los productos"
            });
            if (!productos) return res.status(204).send({
                message: "No se encontraron productos"
            });
            return res.status(200).send({
                productos
            });
        });
    },

    getProductById: function(req, res){
        var productoId = req.params.id;

        Producto.findById(productoId, (error,producto)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error consultando el producto"
            });
            if (!producto) return res.status(204).send({
                message: "No se pudo encontrar el producto con este id" + productoId
            });
            return res.status(200).send({
                producto
            });
        });
    },

    updateProduct: function(req, res){
        var productoId = req.params.id;
        console.log(req.body);
        Producto.findByIdAndUpdate(productoId,req.body, (error,producto)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error editando el producto"
            });
            if (!producto) return res.status(204).send({
                message: "No se pudo editar el producto con este id" + productoId
            });
            return res.status(200).send({
                message: "Producto editado exitosamente" 
            });
        });
    },

    deleteProduct: function(req, res){
        var productoId = req.params.id;
        console.log(req.body);

        Producto.deleteOne({ _id: productoId }, (error,producto)=>{
            if (error) return res.status(500).send({
                message: "Ha ocurrido un error eliminando el producto"
            });
            if (producto.deletedCount == 0) return res.status(204).send({
                message: "No se pudo eliminar el producto con este id" + productoId
            });
            return res.status(200).send({
                message: "Producto eliminado exitosamente" 
            });
        });
    },

    uploadImage: function(req, res){

        if (req.files) {
            var productId = req.params.id;
            var path = req.files.image.path;
            var fSplit = path.split('/');
            var fileName = fSplit[1];
            console.log(path);
            console.log(fileName);
    
            Producto.findByIdAndUpdate(productId, {imageFile:fileName}, (error, productUpdated)=>{
                if (error) return res.status(500).send({
                    message: "Ha ocurrido un error actualizando la imagen"
                });
                if (!productUpdated) return res.status(204).send({
                    message: "No se pudo encontrar el producto con este id" + productId
                });
                return res.status(200).send({
                    message: "El producto con id" + productId + "fue actualizada"
                });         
            });
        } else {
            return res.status(202).send({
                message: 'Procesando la solicitud'
            });
        }
    }
};

module.exports = controller;

