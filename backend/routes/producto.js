//14092022
'use strict'

var express = require('express');
var ProductoController = require ('../controllers/producto');

// Middleware
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({uploadDir:'./uploads'});

var router = express.Router();

router.get('/allProduct',ProductoController.allProduct);
router.get('/getById/:id',ProductoController.getProductById);
router.get('/delete/:id',ProductoController.deleteProduct);
router.post('/save',ProductoController.saveProduct);
router.post('/update/:id',ProductoController.updateProduct);
router.post('/uploadImage/:id',multipartyMiddleware,ProductoController.uploadImage);


module.exports = router;
