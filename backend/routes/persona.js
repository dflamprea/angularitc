'use strict'

var express = require('express');
var PersonaController = require ('../controllers/persona');

var router = express.Router();

// Middleware
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({uploadDir:'./uploads'});

router.get('/home',PersonaController.home);
router.get('/getById/:id',PersonaController.getPersonById);
router.post('/test',PersonaController.test);
router.post('/save',PersonaController.savePerson);
router.post('/uploadImage/:id',multipartyMiddleware,PersonaController.uploadImage)
router.get('/getImageByFileName/:fileName',PersonaController.getImageByFileName)

module.exports = router;
