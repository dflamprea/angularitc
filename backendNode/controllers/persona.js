'use strict'

var Persona = require('../models/persona');
var fileSystem = require('fs');
var path = require('path');
const { exists } = require('../models/persona');

var controller = {

    savePerson: function(req, res) {
        var persona = new Persona();

        var request = req.body;
        persona.names = request.names;
        persona.lastNames = request.lastNames;
        persona.address = request.address;
        persona.phone = request.phone;
        persona.birthDate = request.birthDate;

        persona.save((error, personaStored) => {
            if (error) return res.status(500).send({
                message: 'Ha ocurrido un error almacenando la persona'
            });

            if (!personaStored) return res.status(204).send({
                message: 'No se pudo almacenanar la persona'
            });

            return res.status(200).send({
                persona: personaStored
            });

        });
        
    }, 

    getPersonById: function(req, res) {

        var personId = req.params.id;

        //Persona.findById(personId, (error, persona) => {
            Persona.find( {phone: personId }).exec((error, persona) => {
            
            if (error) {

                console.log(error);

                return res.status(500).send({
                    message: 'Ha ocurrido un error consultado la persona con id: ' + personId
                });
            }

            if (!persona) res.status(204).send({
                message: 'No se encuentra una persona con este id: ' + personId
            });

            return res.status(200).send({
                persona
            });


        });

    }, 

    uploadImage: function(req, res) {

        var personId = req.params.id;

        if (req.files) {

            var path = req.files.image.path;
            var fSplit = path.split('/');
            var fileName = fSplit[1];
            console.log(fileName);

            Persona.findByIdAndUpdate(personId, {imageFile: fileName}, (error, personaUpdated) => {
                
                if (error) return res.status(500).send({
                    message: 'Error actualizando la imagen'
                });

                if (!personaUpdated) return res.status(204).send({
                    message: 'La persona con id: ' + personId + ' no existe'
                });
                
                return res.status(200).send({
                    message: 'La persona con id: ' + personId + ' fue actualizada'
                });

            });

            
        } else {
            return res.status(202).send({
                message: 'Procesando la solicitud'
            });
        }

    },

    getImageByFileName(req, res) {
        var fileName = req.params.fileName;
        var pathFile = './uploads/' + fileName;

        console.log(pathFile);

        fileSystem.exists(pathFile, (exists) => {
            console.log(path);
            console.log(exists);
            if (exists) {
                return res.sendFile(path.resolve(pathFile));
            } else {
                res.status(204).send({
                    message: 'No se encuentra la imagen'
                });
            }
        })
    }

};

module.exports = controller;
