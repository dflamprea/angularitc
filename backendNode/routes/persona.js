'use strict'

var express = require('express');
var PersonaController = require('../controllers/persona');

var router = express.Router();

//Middleware
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty({ uploadDir: './uploads' })

router.post('/save', PersonaController.savePerson);
router.get('/getById/:id', PersonaController.getPersonById);
router.post('/uploadImage/:id', multipartyMiddleware, PersonaController.uploadImage);
router.get('/getImage/:fileName', PersonaController.getImageByFileName);

module.exports = router;