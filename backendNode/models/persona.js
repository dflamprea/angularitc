'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var PersonaSchema = Schema({
    names: String,
    lastNames: String,
    address: String,
    phone: String,
    birthDate: String,
    imageFile: String
});

module.exports = mongoose.model('Persona', PersonaSchema);