'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var projectRoutes = require('./routes/persona');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/api/persona', projectRoutes);

module.exports = app;



