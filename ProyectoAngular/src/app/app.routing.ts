import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AboutComponent } from "./components/about/about.component";
import { ClientsComponent } from "./components/clients/clients.component";
import { ContactusComponent } from "./components/contactus/contactus.component";
import { HomeComponent } from "./components/home/home.component"; 
import { ProductsComponent } from "./components/products/products.component";
import { ServicesComponent } from "./components/services/services.component";
import { NotfoundComponent } from "./components/notfound/notfound.component";

const appRoutes : Routes =  [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'clients', component: ClientsComponent },
    { path: 'contact', component: ContactusComponent },
    { path: 'products', component: ProductsComponent },
    { path: 'services', component: ServicesComponent },
    { path: '**', component: NotfoundComponent }

];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);