import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Config } from "../model/config";


@Injectable()
export class ProductService {

    public urlProducts: String;
    public urlImages: String;

    constructor(public _http: HttpClient ) {
        this.urlProducts = Config.urlProductService;
        this.urlImages = Config.urlImageService;
    }

    getAll() {
        return this._http.get(this.urlProducts + '/allProduct')
    }

    getImage(imagename: string) {
        return this._http.get(this.urlImages + imagename);
    }

}