import { Component, OnInit } from '@angular/core';
import { Products } from '../../model/Product';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductService]
})
export class ProductsComponent implements OnInit {

  public products: Array<any>;

  constructor(private _productService: ProductService) { 
    
    this.products = new Array();

    this.getAllProducts();
    
  }

  getAllProducts() {
    return this._productService.getAll().subscribe(
      result => {
        
        this.products = (result as any).productos;
        this.getImages();
      }, 
      error => {
        console.log(<any>error);
       }
    );
  }

  getImages() {
    let these = this;
    
    this.products.forEach(function(p){
            
      if (p.imageFile) {
        console.log(p.imageFile);

        these._productService.getImage(p.imageFile).subscribe(
          result => {
            console.log(result);
          }, 
          error => {
            console.log(<any>error);
           }
        );
      
      }
  })
  }

  ngOnInit(): void {
  }

}
