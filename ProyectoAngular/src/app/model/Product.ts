export class Products {

    constructor (
        public name: string,
        public description: string,
        public valor: number,
        public year: number,
        public image: string )
        {}

}